import axios from 'axios'
import { createContext, useEffect, useState } from 'react'
import ProductsApi from '../api/ProductsApi'
import UserApi from '../api/UserApi'
import CategoriesAPI from '../api/CategoriesAPI'
// Create a context object to be used by the GlobalState component
export const GlobalState = createContext()

const DataStateProvider = ({ children }) => {
  const [token, setToken] = useState(false)


  useEffect(() => {
    const firstLogin = localStorage.getItem('firstLogin')
    if (firstLogin) {
      const refreshToken = async () => {
        const res = await axios.get('/user/refresh_token')

        setToken(res.data.accesstoken)

        setTimeout(() => {
          refreshToken()
        }, 10 * 60 * 1000)
        console.log(firstLogin)
      }
      refreshToken()

    }
  }, [])

  const state = {
    token: [token, setToken],
    ProductsApi: ProductsApi(),
    UserApi: UserApi(token),
    CategoriesAPI: CategoriesAPI(),
  }

  return <GlobalState.Provider value={state}>{children}</GlobalState.Provider>
}

export default DataStateProvider
