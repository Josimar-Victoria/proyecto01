import React, { useContext } from 'react'
import { Link, NavLink, useNavigate } from 'react-router-dom'

//import icons
import Logo from '../../icons/Logo.png'
import Agregar from '../../icons/agregar.png'
import Menu from '../../icons/menu.png'

//import css modules
import styles from './Header.module.css'
import { GlobalState } from '../../context/GlobalState'
import axios from 'axios'
import Filters from '../product/Filter'

function Header () {
  const state = useContext(GlobalState)
  const [products] = state.ProductsApi.products
  const [search, setSearch] = state.ProductsApi.search
  console.log(products)

  const logoutUser = async () => {
    await axios.get('/user/logout')

    localStorage.removeItem('firstLogin')

    window.location.href = '/'
  }

  console.log(search)
  return (
    <>
      <nav className='navbar navbar-expand-lg navbar-dark bg-black'>
        <div className='container-fluid'>
          <Link className='navbar-brand' to='/'>
            <img src={Logo} alt='' />
          </Link>
          <button
            className='navbar-toggler'
            type='button'
            data-bs-toggle='collapse'
            data-bs-target='#navbarSupportedContent'
            aria-controls='navbarSupportedContent'
            aria-expanded='false'
            aria-label='Toggle navigation'
          >
            <img src={Menu} alt='menu' />
          </button>
          <div className='collapse navbar-collapse' id='navbarSupportedContent'>
            <ul className='navbar-nav me-auto mb-2 mb-lg-0'>
              <li className='nav-item'>
                <NavLink className='nav-link' aria-current='page' to='/'>
                  Home
                </NavLink>
              </li>
            </ul>

            {products.length > 0 && (
              <NavLink to='/create' title='Craer'>
                <img className={styles.agregar} src={Agregar} alt='' />
              </NavLink>
            )}

            <form className='d-flex'>
              <input
                type='search'
                aria-label='Search'
                className='form-control me-2'
                value={search}
                placeholder='¡Search Name¡'
                onChange={e => setSearch(e.target.value.toLowerCase())}
              />
            </form>
          </div>
        </div>
      </nav>
    </>
  )
}

export default Header
