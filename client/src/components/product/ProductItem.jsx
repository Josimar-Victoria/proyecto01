import React from 'react'
import Elimar from '../../icons/eliminar.png'
import Edictar from '../../icons/edictar.png'
import styles from './ProductItem.module.css'
import { Link, useNavigate } from 'react-router-dom'
import axios from 'axios'
function ProductItem ({ product, token, callback, setCallback }) {
  const navigate = useNavigate()

  // delete product
  const deleteProduct = async () => {
    try {
      const detroyImg = axios.post(
        '/api/destroy',
        { public_id: product.images.public_id },
        { headers: { Authorization: token } }
      )
      const deleteProduct = axios.delete(
        `/api/products/${product._id}`,
        { public_id: product.images.public_id },
        { headers: { Authorization: token } }
      )
      await detroyImg
      await deleteProduct
      setCallback(!callback)
      navigate('/')
    } catch (err) {
      alert(err.response.data.message)
    }
    console.log(product)
  }

  return (
    <>
      <tr className='pe-auto' key={product._id}>
        <th scope='row'>
          <Link title='Details' to={`/details/${product._id}`}>
            <img
              className={styles.img_productos}
              src={product.images.url}
              alt={product.name}
            />
          </Link>
        </th>
        <td>{product.name}</td>
        <td>{product.category}</td>
        <td>{product.price}</td>
        <td>{product.amount}</td>
        <td>Otto</td>

        <td>
          <button type='button' className='btn ' onClick={deleteProduct}>
            <img src={Elimar} alt='eliminar' />
          </button>
          {''}
          <Link to={`/edit_product/${product._id}`}>
            <img src={Edictar} alt='' />
          </Link>
        </td>
      </tr>
    </>
  )
}

export default ProductItem
