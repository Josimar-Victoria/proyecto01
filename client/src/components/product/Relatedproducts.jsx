import { Link } from 'react-router-dom'

function Relatedproducts ({ product }) {
  return (
    <div className='card mb-3' style={{ maxWidth: '540px' }}>
      <div className='row g-0'>
        <div className='col-md-4'>
          <Link to={`/details/${product._id}`}>
            <img
              src={product.images.url}
              className='img-fluid rounded-start'
              alt='...'
            />
          </Link>
        </div>
        <div className='col-md-8'>
          <div className='card-body m-2'>
            <h5 className='card-title'>{product.name}</h5>
            <p className='card-text'>
              <small className='text-muted'>Categoria:</small>{' '}
              {product.category}
            </p>
            <p className='card-text'>
              <small className='text-muted'>Precio:</small>
              {product.price}
            </p>
            <p className='card-text'>{product.description}</p>
            <p className='card-text'>
              <small className='text-muted'>Last updated 3 mins ago</small>
            </p>
          </div>
        </div>
      </div>
    </div>
  )
}

export default Relatedproducts
