import React from 'react'
import ReactDOM from 'react-dom/client'
import './index.css'
import App from './App'
import { BrowserRouter } from 'react-router-dom'
import DataStateProvider from './context/GlobalState'

const root = ReactDOM.createRoot(document.getElementById('root'))
root.render(
  <DataStateProvider>
    <BrowserRouter>
      <App />
    </BrowserRouter>
  </DataStateProvider>
)
