import { Route, Routes } from 'react-router-dom'
import Header from './components/header/Header'
import Home from './pages/home/Home'
import Login from './pages/login/Login'
import DetailsProducts from './pages/details/DetailsProducts'
import Register from './pages/register/Register'
import CreateProduct from './pages/create/CreateProduct'

function App () {
  return (
    <>
      <Header />
      <Routes>
        <Route path='/' element={<Home />} />
        <Route path='/details/:id' element={<DetailsProducts />} />
        <Route path='/create' element={<CreateProduct />} />
        <Route path='/edit_product/:id' element={<CreateProduct />} />
        <Route path='/login' element={<Login />} />
        <Route path='/register' element={<Register />} />
      </Routes>
    </>
  )
}

export default App
