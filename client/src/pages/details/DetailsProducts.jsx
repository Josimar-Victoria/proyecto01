import React, { useContext, useEffect, useState } from 'react'
import {  useParams } from 'react-router-dom'
import Relatedproducts from '../../components/product/Relatedproducts.jsx'
import { GlobalState } from '../../context/GlobalState'

import styles from './Deatail.module.css'

function DetailsProducts () {
  const state = useContext(GlobalState)
  const [products] = state.ProductsApi.products
  const [detailProduct, setdetailProduct] = useState([])
  const params = useParams()

  useEffect(() => {
    if (params.id) {
      const product = products.find(product => product._id === params.id)
      setdetailProduct(product)
    }
  }, [products, params.id])

  if (detailProduct.length === 0) return null

  return (
    <>
      <div className={styles.detail}>
        <img src={detailProduct.images.url} alt='' />
        <div className={styles.box_detail}>
          <div className={styles.row}>
            <h2>{detailProduct.title}</h2>
            <h2>#id: {detailProduct.product_id}</h2>
          </div>
          <span>${detailProduct.price}</span>
          <p>{detailProduct.description}</p>
          <p>{detailProduct.content}</p>
          <p>Cantidad: {detailProduct.amount}</p>
        </div>
      </div>
      <div className=''>
        <h2>Productos relacionados</h2>
        <div className={styles.products}>
          {products.map(product => {
            return product.category === detailProduct.category ? (
              <Relatedproducts key={product._id} product={product} />
            ) : null
          })}
        </div>
      </div>
    </>
  )
}

export default DetailsProducts
