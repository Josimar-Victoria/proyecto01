import axios from 'axios'
import React, { useContext, useEffect } from 'react'
import Agregar from '../../icons/agregar.png'

import ProductItem from '../../components/product/ProductItem'
import { GlobalState } from '../../context/GlobalState'
import { Link } from 'react-router-dom'
import Filters from '../../components/product/Filter'

function Home () {
  const state = useContext(GlobalState)
  const [token] = state.token
  const [callback, setCallback] = state.ProductsApi.callback
  const [products, setProduct] = state.ProductsApi.products
  

  useEffect(() => {
    const getProducts = async () => {
      const res = await axios.get('/api/products')
      setProduct(res.data.products)
    }
    getProducts()
  }, [setProduct, callback])

  return (
    <>
    <Filters />
      <table className='table table-hover container'>
        <thead>
          <tr>
            <th scope='col'>Product</th>
            <th scope='col'>Name</th>
            <th scope='col'>Categories</th>
            <th scope='col'>Price</th>
            <th scope='col'>Amount</th>
            <th scope='col'>Inventory</th>
            <th scope='col'>Action</th>
          </tr>
        </thead>

        <tbody>
          {products.map(product => (
            <ProductItem
              key={product._id}
              product={product}
              token={token}
              callback={callback}
              setCallback={setCallback}
            />
          ))}
        </tbody>
      </table>

      {products.length === 0 && (
        <div className='container flex '>
          <Link to='/create'>
            <img src={Agregar} alt='' />
          </Link>
        </div>
      )}
    </>
  )
}

export default Home
