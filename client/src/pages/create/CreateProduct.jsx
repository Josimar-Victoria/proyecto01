import axios from 'axios'
import React, { useContext, useEffect, useState } from 'react'
import { useNavigate, useParams } from 'react-router-dom'
import Loading from '../../components/loading/Loading'
import { GlobalState } from '../../context/GlobalState'

import './create.css'

const initialState = {
  product_id: '',
  name: '',
  price: 0,
  description:
    'lorem ipsum dolor sit amet dolor sit amet, consectetur adipiscing elit, sed do eiusmod ',
  content: 'Lorem ipsum dolor sit amet dolor sit amet dolor ',
  image: '',
  category: '',
  amount: 3
}
export default function CreateProduct () {
  const state = useContext(GlobalState)
  const [product, setProduct] = useState(initialState)
  const [categories] = state.CategoriesAPI.categories
  const [products] = state.ProductsApi.products
  const [callback, setCallback] = state.ProductsApi.callback
  const [images, setImages] = useState(false)
  const [loading, setLoading] = useState(false)
  const [onEdit, setOnEdit] = useState(false)

  const [token] = state.token

  const navigation = useNavigate()
  const params = useParams()

  useEffect(() => {
    if (params.id) {
      setOnEdit(true)
      products.forEach(product => {
        if (product._id === params.id) {
          setProduct(product)
          setImages(product.images)
        }
      })
    } else {
      setOnEdit(false)
      setProduct(initialState)
      setImages(false)
    }
  }, [params.id, products])

  const handleUpload = async e => {
    e.preventDefault()
    try {
      const file = e.target.files[0]

      if (!file) return alert('El archivo no existe')

      if (file.size > 1024 * 1024) return alert('¡Tamaño demasiado grande!') //1mb

      if (file.type !== 'image/jpeg' && file.type !== 'image/png')
        return alert('El formato de archivo es incorrecto')
      let formData = new FormData()
      formData.append('file', file)
      setLoading(true)

      const res = await axios.post('/api/upload', formData, {
        headers: { 'content-type': 'multipart/form-data', Authorization: token }
      })
      setLoading(false)
      setImages(res.data)
    } catch (error) {
      alert(error.response.data.msg)
    }
  }

  const handleDestroy = async () => {
    try {
      setLoading(true)
      await axios.post(
        '/api/destroy',
        { public_id: images.public_id },
        {
          headers: { Authorization: token }
        }
      )
      setLoading(false)
      setImages(false)
    } catch (error) {
      alert(error.response.data.msg)
    }
  }

  const handleChangeInput = e => {
    const { name, value } = e.target
    setProduct({ ...product, [name]: value })
  }

  const handleSubmit = async e => {
    e.preventDefault()
    try {
      if (!images) return alert('Sin actualización de imagen')
      if (onEdit) {
        await axios.put(
          `/api/products/${product._id}`,
          { ...product, images },
          {
            headers: { Authorization: token }
          }
        )
      } else {
        await axios.post(
          '/api/products',
          { ...product, images },
          {
            headers: { Authorization: token }
          }
        )
      }
      setCallback(!callback)
      navigation('/')
    } catch (error) {
      alert(error.response.data.msg)
    }
  }

  const styleUpload = {
    display: images ? 'block' : 'none'
  }

  return (
    <div className='create_product'>
      <div className='upload'>
        <input type='file' name='file' id='file_up' onChange={handleUpload} />
        {loading ? (
          <div id='file_img'>
            <Loading />
          </div>
        ) : (
          <div id='file_img' style={styleUpload}>
            <img src={images ? images.url : ''} alt='' />
            <span onClick={handleDestroy}>X</span>
          </div>
        )}
      </div>

      <form onSubmit={handleSubmit}>
        <div className='row'>
          <label htmlFor='product_id'>Product_id</label>
          <input
            type='text'
            name='product_id'
            id='product_id'
            onChange={handleChangeInput}
            value={product.product_id}
            disabled={onEdit}
            required
          />
        </div>
        <div className='row'>
          <label htmlFor='title'>Name</label>
          <input
            type='text'
            name='name'
            id='name'
            onChange={handleChangeInput}
            value={product.name}
            required
          />
        </div>
        <div className='row'>
          <label htmlFor='price'>Price</label>
          <input
            type='number'
            name='price'
            id='price'
            required
            onChange={handleChangeInput}
            value={product.price}
          />
        </div>

        <div className='row'>
          <label htmlFor='price'>Amount</label>
          <input
            type='number'
            name='amount'
            id='amount'
            onChange={handleChangeInput}
            required
            value={product.amount}
          />
        </div>
        <div className='row'>
          <label htmlFor='description'>Description</label>
          <textarea
            type='text'
            name='description'
            id='description'
            onChange={handleChangeInput}
            value={product.description}
            rows='5'
            required
          />
        </div>
        <div className='row'>
          <label htmlFor='content'>Content</label>
          <textarea
            type='text'
            name='content'
            id='content'
            rows='7'
            onChange={handleChangeInput}
            value={product.content}
            required
          />
        </div>
        <div className='row'>
          <label htmlFor='category'>Categories</label>
          <select
            name='category'
            id='category'
            value={product.category}
            onChange={handleChangeInput}
          >
            <option value=''>please select a Category</option>
            {categories.map(category => (
              <option value={category.name} key={category._id}>
                {category.name}
              </option>
            ))}
          </select>
        </div>

        <button type='submit'>{onEdit ? 'Update' : 'Create'}</button>
      </form>
    </div>
  )
}
