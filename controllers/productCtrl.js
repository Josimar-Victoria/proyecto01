const Products = require('../models/productModel')

class APIfeatures {
  constructor (query, queryString) {
    this.query = query
    this.queryString = queryString
  }

  filtering () {
    const queryObj = { ...this.queryString } //queryString = req.query

    const excludedFields = ['page', 'sort', 'limit']
    excludedFields.forEach(el => delete queryObj[el])

    let queryStr = JSON.stringify(queryObj)
    queryStr = queryStr.replace(
      /\b(gte|gt|lt|lte|regex)\b/g,
      match => '$' + match
    )

    //    gte = mayor que o igual
    //    lte = menor o igual que
    //    lt = menor que
    //    gt = mas grande que
    this.query.find(JSON.parse(queryStr))

    return this
  }
  sorting () {
    if (this.queryString.sort) {
      const sortBy = this.queryString.sort.split(',').join(' ')
      this.query = this.query.sort(sortBy)
    } else {
      this.query = this.query.sort('-createdAt')
    }

    return this
  }
  paginating () {
    const page = this.queryString.page * 1 || 1
    const limit = this.queryString.limit * 1 || 9
    const skip = (page - 1) * limit
    this.query = this.query.skip(skip).limit(limit)
    return this
  }
}

const productCtrl = {
  getProducts: async (req, res) => {
    try {
      const feature = new APIfeatures(Products.find(), req.query)
        .filtering()
        .sorting()
        .paginating()

      const products = await feature.query
      res.json({
        status: 'success',
        result: products.length,
        products: products
      })
    } catch (err) {
      res.status(500).json({ msg: err.message })
    }
  },
  createProduct: async (req, res) => {
    try {
      const {
        product_id,
        name,
        price,
        description,
        content,
        images,
        category,
        amount
      } = req.body
      if (!images) return res.status(400).json({ msg: 'Sin carga de imagen' })

      const product = await Products.findOne({ product_id })
      if (product)
        return res.status(400).json({ msg: 'Este producto ya existe.' })

      const newProduct = new Products({
        product_id,
        name: name.toLowerCase(),
        price,
        description,
        content,
        images,
        category,
        amount
      })

      await newProduct.save()
      res.json({ msg: 'Creó un producto' })
    } catch (err) {
      return res.status(500).json({ msg: err.message })
    }
  },
  deleteProduct: async (req, res) => {
    try {
      const product = await Products.findByIdAndDelete(req.params.id)
      res.json({ msg: 'El producto Eliminado' })
    } catch (err) {
      res.status(500).json({ msg: err.message })
    }
  },
  updateProduct: async (req, res) => {
    try {
      const {
        name,
        price,
        amount,
        description,
        content,
        image,
        category
      } = req.body
      //  if (!image) return res.status(400).json({ msg: 'No image Upluad' })

    await Products.findByIdAndUpdate(
        { _id: req.params.id },
        {
          name: name.toLowerCase(),
          price,
          description,
          content,
          image,
          category,
          amount
        }
      )

      res.json({ msg: 'producto Actualización' })
    } catch (err) {
      res.status(500).json({ msg: err.message })
    }
  }
}

module.exports = productCtrl
