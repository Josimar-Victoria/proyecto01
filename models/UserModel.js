const mongoose = require('mongoose')

const UserSchema = new mongoose.Schema(
  {
    name: {
      type: String,
      required: true,
      trim: true
    },
    email: {
      type: String,
      unique: true
    },
    password: {
      type: String,
      required: true
    },
    role: {
      type: Number,
      default: false
    },
    cart: {
      type: Array,
      default: []
    }
  },
  {
    timestamps: true
  }
)

module.exports = User = mongoose.model('user', UserSchema)
