require('dotenv').config()
const express = require('express')
const mongoose = require('mongoose')
const cors = require('cors')
const fileUpload = require('express-fileupload')
const bodyParser = require('body-parser')

const app = express()

app.use(express.json())
app.use(bodyParser.json())

app.use(cors())
app.use(
  fileUpload({
    useTempFiles: true
  })
)

// Router
app.use('/user', require('./routes/UserRouter'))
app.use('/api', require('./routes/CategoryRouer'))
app.use('/api', require('./routes/upload'))
app.use('/api', require('./routes/productRouter'))

// Connect to MongoDB
const URI = process.env.MONGODB_URI

mongoose.connect(
  URI,
  {
    useNewUrlParser: true,
    useUnifiedTopology: true
  },
  err => {
    if (err) throw err
    console.log('MongoDB connected')
  }
)

app.get('/', (req, res) => {
  res.json({ msg: 'Hello World' })
})

const PORT = process.env.PORT || 5500

app.listen(PORT, () => {
  console.log(`Server running on port http://localhost:${PORT}`)
})
